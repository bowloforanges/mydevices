package com.softtek.course.DBtest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBConnection {

    public String dbUsername = "root";
    public String dbPassword = "1234";
    public String dbSchema = "jdbc:mysql://127.0.0.1:3306/session3?serverTimezone=UTC#"; /*?serverTimezone=UTC#*/
    public String simpleQuery = "SELECT * FROM device;";

    public void DBConnection() {
    }

    public void connect() {
        
        System.out.println("MySQL JDBC Connection");

        //List<Device> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(dbSchema, dbUsername, dbPassword)) {

            if (conn != null) {
                System.out.println("Connected to the database!");

                PreparedStatement preparedStatement = conn.prepareStatement(simpleQuery);

                ResultSet resultSet = preparedStatement.executeQuery();

                List<Device> list = new ArrayList<>(); // LISTA

                while (resultSet.next()) { // ITERABLE

                    list.add(new Device(resultSet.getLong("DEVICEID"), resultSet.getString("NAME"),
                            resultSet.getString("DESCRIPTION"), resultSet.getLong("MANUFACTURERID"),
                            resultSet.getLong("COLORID"), resultSet.getString("COMMENTS")));
                }

                //System.out.println("first print...");

                for (Device d : list) { // Printing Table
                    System.out.println(
                            "ID: " + d.getDeviceId() + 
                            ", NAME: " + d.getName() + 
                            ", DESCRIPTION: " + d.getDescription() + 
                            ", MANUFACTURERID: " + d.getManufacturerId() + 
                            ", COLORID: " + d.getColorId() + 
                            ", COMMENTS: " + d.getComments()
                            );
                }

                /*System.out.println("second print...");

                list.forEach(System.out::println); // Printing Table*/

            } else {
                System.out.println("Failed to make connection!");
            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
