package com.softtek.course.DBtest;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DBSingleton {

    private static Connection conn = null;

    private DBSingleton() {

        String dbUrl = "jdbc:mysql://127.0.0.1:3306/session3?serverTimezone=UTC#";
        String dbDriver = "com.mysql.jdbc.Driver";
        String dbUsername = "root";
        String dbPassword = "1234";

        try {

            System.out.println("Connecting to database with SINGLETON...");
            Class.forName(dbDriver);
            conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {

        if (conn == null) {
            new DBSingleton();
        }

        return conn;

    }

    public static void closeConnection() throws SQLException {
        if (conn != null) {
            conn.close();
        }
    }

    public static List<Device> executeQuery(String simpleQuery) throws SQLException {
        
        List<Device> list = new ArrayList<>(); // LISTA

        if (conn != null) {

            System.out.println("Connected to the database!");

            PreparedStatement preparedStatement = conn.prepareStatement(simpleQuery);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) { // ITERABLE

                list.add(new Device(resultSet.getLong("DEVICEID"), resultSet.getString("NAME"),
                        resultSet.getString("DESCRIPTION"), resultSet.getLong("MANUFACTURERID"),
                        resultSet.getLong("COLORID"), resultSet.getString("COMMENTS")));
            }

            return list;

        } else {
            System.out.println("Failed to fetch query...");
            return null;
        }
    }

}
