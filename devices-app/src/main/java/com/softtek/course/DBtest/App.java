package com.softtek.course.DBtest;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) throws SQLException {
        List<Device> queryResult = new ArrayList<>();
        String simpleQuery = "SELECT * FROM device;";

        DBSingleton.getConnection();
        queryResult = DBSingleton.executeQuery(simpleQuery);
        DBSingleton.closeConnection();

        System.out.println("Query Result:");

        for (Device d : queryResult) { // Printing Table
            System.out.println("ID: " + d.getDeviceId() + 
                            ", NAME: " + d.getName() + 
                            ", DESCRIPTION: " + d.getDescription() + 
                            ", MANUFACTURERID: " + d.getManufacturerId() + 
                            ", COLORID: " + d.getColorId() + 
                            ", COMMENTS: " + d.getComments()
                            );
        }

        System.out.println("------------------ STREAM 1 -------------------");

        List<String> stream1ToList = queryResult.stream() //List<Device>
            .map(Device::getName)
            .filter(name -> name.equals("Laptop"))
            .collect(Collectors.toList());
            //.forEach(System.out::println);

        for (String string : stream1ToList) {
            System.out.println(string);
        }

        System.out.println("------------------ STREAM 2 -------------------");

        Long sumatoriaId = queryResult.stream() 
            .map(Device::getManufacturerId)
            .filter(manId -> manId == 3)
            .count();

        System.out.println("Number of devices with manufacturerId = 3: " + sumatoriaId + "\n");

        List<Device> colorIdList = queryResult.stream()
            .filter(dev -> dev.getColorId() == 1)
            .collect(Collectors.toList());

        for (Device dev : colorIdList) {
            System.out.println("device with colorId = 1 : " + dev.getName() + " with id: " + dev.getColorId());
        }

        System.out.println("------------------ STREAM 3 -------------------");

        Map<Long, List<Device>> lastMapping = queryResult.stream()
            .collect(Collectors.groupingBy(Device::getDeviceId));

        System.out.println("Mapping:");
        lastMapping.forEach((k, v) -> System.out.println(k + " : " + v.toString()));

        // Falta arreglar el print de value aquí, porque imprime el objeto.


    }

}